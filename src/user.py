##############
# USER CLASS #
##############

import math


class User:
    SYMPTOMATIC = 1
    CONTACT = 2
    HOSPITALIZATION = 3
    NURSING_STAFF = 4
    SECOND_TEST = 5
    PERSONAL = 6

    def __init__(self, nid, surname, name, coordinates1, coordinates2, reason, severity, has_appointment):
        self._nid = nid
        self._surname = surname
        self._name = name
        self._coordinates = [coordinates1, coordinates2]
        self._reason = reason
        self._severity = severity
        self._has_appointment = has_appointment
        self._preferred_slots = []

    def user_preferred_slots(self, centers):
        """
        Order the available slots for each user. The slots are firstly ordered regarding the distance
        between the center they belong to and the location specified by the user.
        Among the slots belonging to the same center, they are secondly ordered in ascending order of delay.

        :param centers: centers available to get a PCR test
        """
        r = 6372.795477598
        # Location entered by the user
        long_user = self._coordinates[0]
        lat_user = self._coordinates[1]

        distances = []
        for center in centers:
            # Location of the center
            long_center = center.coordinates[0]
            lat_center = center.coordinates[1]
            # Haversine formula to compute the distance between 2 geographical coordinates
            distance = 2 * r * math.asin((math.sin((long_center - long_user)/2))**2 + math.cos(long_user) *
                                         math.cos(long_center) * (math.sin((lat_center - lat_user)/2))**2)
            distances.append((center, distance))
        # Order the centers according to its distance to the user's location
        sorted_distances = sorted(distances, key=lambda dist: dist[1])

        # For each center, order the slots according to their delays
        for i in range(len(sorted_distances)):
            slots = sorted_distances[i][0].slots_sorted()
            # Add the slots to the preferred_slots list by order of closest center and shorter delay
            for slot in slots:
                self._preferred_slots.append(slot)

    # Getters and Setters
    @property
    def name(self):
        return self._name

    @property
    def coordinates(self):
        return self._coordinates

    @property
    def reason(self):
        return self._reason

    @property
    def severity(self):
        return self._severity

    @property
    def preferred_slots(self):
        return self._preferred_slots

    @property
    def has_appointment(self):
        return self._has_appointment

    @has_appointment.setter
    def has_appointment(self, value):
        self._has_appointment = value
