#####################
# APPOINTMENT CLASS #
#####################

class Appointment:

    def __init__(self, user, slot):
        self._user = user
        self._slot = slot

    def appointment_made(self):
        """
        Update the corresponding attributes of the slot and user related to the appointment made.
        """
        self.slot.availability = False
        self.user.has_appointment = True

    def appointment_undone(self):
        """
        Update the corresponding attributes of the slot and user related to the appointment undone.
        """
        self.slot.availability = True
        self.user.has_appointment = False

    # Getters and Setters
    @property
    def user(self):
        return self._user

    @property
    def slot(self):
        return self._slot

    @user.setter
    def user(self, u):
        self._user = u

    @slot.setter
    def slot(self, s):
        self._slot = s
