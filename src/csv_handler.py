#########################################################################################
# CSV HANDLER CLASS                                                                     #
# This class deals with the csv files containing the data related to the program.       #
# In this implementation, 4 files are used recording information about, for the         #
# first one, users, the second, centres, the third, slots, and the last, appointments.  #
# Functions of the class allow to retrieve data from these files and to update the      #
# files if the data are modified.                                                       #
#########################################################################################


import csv
from pandas import read_csv
from user import User
from slot import Slot
from center import Center


class CsvHandler:

    def __init__(self, user_file, slot_file, center_file, appointment_file):
        self._filenames = [user_file, center_file, slot_file, appointment_file]
        self._columns = [['nid', 'surname', 'name', 'geojson1', 'geojson2', 'reason', 'severity', 'processed'],
                         ['name', 'geojson1', 'geojson2', 'price', 'result_time'],
                         ['name', 'center', 'availability', 'year', 'month', 'day', 'hour', 'minute'],
                         ['user', 'slot', 'center']]

    def user_data(self):
        """
        Retrieve the data of unprocessed users from the corresponding csv file.
        Unprocessed users are users that are looking for an appointment (False value in the 'processed'
        column) and in general csv file can contain processed and unprocessed users.

        :return: list of user objects that are looking for an appointment
        """
        data = read_csv(self._filenames[0])
        data.columns = self._columns[0]
        users = []
        for i in range(len(data)):
            if data['processed'][i] == 0:
                user = User(data['nid'][i], data['surname'][i], data['name'][i], data['geojson1'][i],
                            data['geojson2'][i], data['reason'][i], data['severity'][i], bool(data['processed'][i]))
                users.append(user)

        return users

    def center_data(self):
        """
        Retrieve the data of centers from the corresponding csv file.

        :return: list of center objects
        """
        data = read_csv(self._filenames[1])
        data.columns = self._columns[1]
        centers = []
        for i in range(len(data)):
            center = Center(data['name'][i], [data['geojson1'][i], data['geojson2'][i]], [], data['price'][i],
                            data['result_time'][i])
            centers.append(center)

        return centers

    def slot_data(self, centers):
        """
        Retrieve the data of available slots from the corresponding csv file.
        In general csv file can contain available and non available slots.

        :param centers: list of existing centers to which slots may belong to
        :return: list of slots objects available for an appointment
        """
        data = read_csv(self._filenames[2])
        data.columns = self._columns[2]
        slots = []
        for center in centers:
            for i in range(len(data)):
                if data['center'][i] == center.name and data['availability'][i] == 1:
                    slot = Slot(data['name'][i], center, bool(data['availability'][i]), data['year'][i],
                                data['month'][i], data['day'][i], data['hour'][i], data['minute'][i])
                    if slot not in center.slots:
                        center.slots.append(slot)
                    slots.append(slot)

        return slots

    def change_processed(self, users, processed):
        """
        Update the value of the 'processed' column in the users csv file.

        :param users: list of users for which the value of the 'processed' column might be changed
        :param processed: value to which the 'processed' column will be changed to
        """
        r = read_csv(self._filenames[0])
        r.columns = self._columns[0]
        writer = csv.writer(open(self._filenames[0], 'w'))
        writer.writerow(self._columns[0])
        for i in range(len(r)):
            for user in users:
                if r['name'][i] == user.name:
                    if int(r['processed'][i] != int(processed)):
                        r['processed'][i] = int(processed)
            writer.writerow([r['nid'][i], r['surname'][i], r['name'][i], r['geojson1'][i], r['geojson2'][i],
                             r['reason'][i], r['severity'][i], r['processed'][i]])

    def change_availability(self, slots, availability):
        """
        Update the value of the 'availability' column in the slots csv file.

        :param slots: list of slots for which the value of the 'availability' column might be changed
        :param availability: value to which the 'availability' column will be changed to
        """
        r = read_csv(self._filenames[2])
        r.columns = self._columns[2]
        writer = csv.writer(open(self._filenames[2], 'w'))
        writer.writerow(self._columns[2])
        for i in range(len(r)):
            for slot in slots:
                if r['name'][i] == slot.name:
                    if int(r['availability'][i] != int(availability)):
                        r['availability'][i] = int(availability)
            writer.writerow([r['name'][i], r['center'][i], r['availability'][i], r['year'][i], r['month'][i],
                             r['day'][i], r['hour'][i], r['minute'][i]])

    def write_appointment_to_file(self, appointment):
        """
        Add an appointment to the appointments csv file.

        :param appointment: appointment to add to the file
        """
        line = [appointment.user.name, appointment.slot.name, appointment.slot.center.name]
        with open(self._filenames[3], 'a', newline='') as write_obj:
            writer = csv.writer(write_obj)
            writer.writerow(line)
