##############################################################################
# MAIN CLASS                                                                 #
# This class launches the program which goal is to create an intelligent     #
# scheduling of PCR test appointments, the ultimate goal of the development  #
# being to accelerate and optimise the testing process and thus to limit     #
# the spread of COVID-19.                                                    #
# Here is the usage :                                                        #
# main.py [-h] user_file slot_file center_file appointment_file              #
##############################################################################

import argparse
from csv_handler import CsvHandler
from scheduling import Scheduling


def main():
    # Arguments management using the argparse module
    parser = argparse.ArgumentParser(description='Appointment maker for PCR tests.')
    parser.add_argument("user_file", help="users CSV file to be used", type=str)
    parser.add_argument("slot_file", help="slots CSV file to be used", type=str)
    parser.add_argument("center_file", help="centers CSV file to be used", type=str)
    parser.add_argument("appointment_file", help="appointments CSV file to be used", type=str)
    args = parser.parse_args()

    # First retrieve the data, stored in csv files
    handler = CsvHandler(args.user_file, args.slot_file, args.center_file, args.appointment_file)
    centers = handler.center_data()
    slots = handler.slot_data(centers)
    users = handler.user_data()

    # Establish the lists of preferences and priorities
    for user in users:
        user.user_preferred_slots(centers)
    for slot in slots:
        slot.slot_priority_users(users)

    # Schedule the appointments
    schedule = Scheduling(users, slots)

    # Update the csv files according to the scheduling made
    for appointment in schedule.current:
        handler.write_appointment_to_file(appointment)
    handler.change_processed(users, True)
    handler.change_availability(slots, False)

    # Assign restart to True if you want to be able to redo the scheduling with the same users and
    # slots later on. It will put back the attributes 'processed' to False and 'availability' to True.
    restart = False
    if restart:
        handler.change_processed(users, False)
        handler.change_availability(slots, True)


if __name__ == '__main__':
    main()
