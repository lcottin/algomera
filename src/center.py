################
# CENTER CLASS #
################

class Center:

    def __init__(self, name, coordinates, slots, price, result_time):
        self._name = name
        self._coordinates = coordinates
        self.slots = slots
        self._price = price
        self._result_time = result_time

    def slots_sorted(self):
        """
        Order the slots for each center in ascending order of delays
        """
        center_slot_sorted = sorted(self.slots, key=lambda slot: slot.delay)
        return center_slot_sorted

    # Getters and Setters
    @property
    def name(self):
        return self._name

    @property
    def coordinates(self):
        return self._coordinates
