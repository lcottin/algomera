################################################################################
# SCHEDULING CLASS                                                             #
# This class creates the scheduling of appointments based on the Gale-Shapley  #
# stable marriage algorithm.                                                   #
################################################################################

from appointment import Appointment


class Scheduling:

    def __init__(self, users, slots, verbose=None):
        if len(users) == 0 and len(slots) == 0:
            print('All appointments have already been made')
            exit(1)
        elif len(users) > len(slots):
            print('Not enough slots available')
            exit(1)

        self._current = []
        self._all_have_appointments = []
        for user in users:
            self._all_have_appointments.append(user.has_appointment)

        self._verbose = verbose
        if verbose:
            self.stable_appointment_v(users)
        else:
            self.stable_appointment(users)

    def stable_appointment(self, users):
        """
        Implement the Gale-Shapley algorithm to find a stable schedule of appointments.

        :param users: list of users that are looking for an appointment
        """
        while not all(self._all_have_appointments):
            for user in users:
                i = 0
                while not user.has_appointment:
                    # Slots are explored by order of preference
                    s = user.preferred_slots[i]

                    # If the slot is available, the appointment is made
                    if s.availability:
                        self.make_appointment(users, user, s)
                        # When all appointments have been made, we can return
                        if all(self._all_have_appointments):
                            return

                    # If not, the priorities are looked at
                    else:
                        # Retrieve the appointment previously made with the concerned slot
                        prev_appointment = None
                        for a in self._current:
                            if a.slot == s:
                                prev_appointment = a
                                break
                        if prev_appointment is None:
                            exit(1)
                        # If the current has higher priority, the previous appointment is undone and a new
                        # appointment is made with the current user and the slot
                        if s.priority_users.index(prev_appointment.user) > s.priority_users.index(user):
                            self.undo_appointment(users, prev_appointment)
                            self.make_appointment(users, user, s)
                        # If not, the next preferred slot will be considered
                        else:
                            i += 1

    def stable_appointment_v(self, users):
        """
        Implement the Gale-Shapley algorithm to find a stable schedule of appointments.
        Verbose version.

        :param users: list of users that are looking for an appointment
        """
        while not all(self._all_have_appointments):
            for user in users:
                print("-------------------- User being processed :", user.name, "--------------------")
                i = 0
                while not user.has_appointment:
                    # Slots are explored by order of preference
                    s = user.preferred_slots[i]
                    print("# Selected slot :", s.name)

                    # If the slot is available, the appointment is made
                    if s.availability:
                        self.make_appointment(users, user, s)
                        print("The slot is available, an appointment is made :", user.name, s.name)
                        # When all appointments have been made, we can return
                        if all(self._all_have_appointments):
                            return

                    # If not, the priorities are looked at
                    else:
                        print("Unavailable slot.")
                        # Retrieve the appointment previously made with the concerned slot
                        prev_appointment = None
                        for a in self._current:
                            if a.slot == s:
                                prev_appointment = a
                                break
                        if prev_appointment is None:
                            print("Error. No previous appointment made.")
                            exit(1)
                        # If the current has higher priority, the previous appointment is undone and a new
                        # appointment is made with the current user and the slot
                        if s.priority_users.index(prev_appointment.user) > s.priority_users.index(user):
                            print("Current user has priority, previous appointment is undone :",
                                  prev_appointment.user.name, prev_appointment.slot.name)
                            print("and new appointment is made:", user.name, s.name)
                            self.undo_appointment(users, prev_appointment)
                            self.make_appointment(users, user, s)
                        # If not, the next preferred slot will be considered
                        else:
                            print("Current user has lower priority, the next slot is selected.")
                            i += 1

    def make_appointment(self, users, user, slot):
        """
        Create an appointment and add it to the current schedule.

        :param users: list of all users looking to an appointment (to update the all_have_appointments list)
        :param user: user for which the appointment is made
        :param slot: slot for which the appointment is made
        """
        appointment = Appointment(user, slot)
        appointment.appointment_made()
        self._current.append(appointment)
        self._all_have_appointments[users.index(user)] = True

    def undo_appointment(self, users, appointment):
        """
        Cancel an appointment and remove it to the current schedule.

        :param users: list of all users looking to an appointment (to update the all_have_appointments list)
        :param appointment: appointment to be canceled and removed
        """
        appointment.appointment_undone()
        self._current.remove(appointment)
        self._all_have_appointments[users.index(appointment.user)] = False

    # Getter
    @property
    def current(self):
        return self._current
