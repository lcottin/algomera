##############
# SLOT CLASS #
##############

import pandas
from datetime import datetime
pandas.options.mode.chained_assignment = None


class Slot:

    def __init__(self, name, center, availability, year, month, day, hour, minute):
        self._name = name
        self._center = center
        self._availability = availability
        self._year = year
        self._month = month
        self._day = day
        self._hour = hour
        self._minute = minute
        self._delay = 0
        self._priority_users = []
        self.compute_delay()

    def compute_delay(self):
        """
        Compute the waiting time between the actual time and the time of the slot
        """
        dt = datetime(self._year, self._month, self._day, self._hour, self._minute)
        timestamp = dt.replace().timestamp()  # timestamp du slot
        datetime_now = datetime.now()
        timestamp_now = datetime_now.replace().timestamp()  # actual timestamp
        self._delay = int(timestamp - timestamp_now)

    def slot_priority_users(self, users):
        """
        Order users for each slot. The users are firstly ordered regarding their priority to the slot.
        Among the users belonging to the same category of patient, they are secondly ordered in ascending
        order of severity.

        :param users: users that want a PCR test
        """
        priorities = [[], [], [], [], [], []]
        for user in users:
            if user.reason == user.SYMPTOMATIC:
                priorities[0].append(user)
            elif user.reason == user.CONTACT:
                priorities[1].append(user)
            elif user.reason == user.HOSPITALIZATION:
                priorities[2].append(user)
            elif user.reason == user.NURSING_STAFF:
                priorities[3].append(user)
            elif user.reason == user.SECOND_TEST:
                priorities[4].append(user)
            elif user.reason == user.PERSONAL:
                priorities[5].append(user)

        # Each list is first ordered by severity and then added to the priority_users list by order of priority
        for list in priorities:
            list = sorted(list, key=lambda user: user.severity)
            self._priority_users += list

    # Getters and Setters
    @property
    def name(self):
        return self._name

    @property
    def center(self):
        return self._center

    @property
    def availability(self):
        return self._availability

    @availability.setter
    def availability(self, value):
        self._availability = value

    @property
    def delay(self):
        return self._delay

    @property
    def priority_users(self):
        return self._priority_users
